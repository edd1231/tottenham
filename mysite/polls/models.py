# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.shortcuts import get_object_or_404
from django.db import models
from django.utils import timezone
from players.models import Player

# Create your models here.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __str__(self):
        return self.question_text
    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __str__(self):
        return self.choice_text


def topChoice(key):
    """
    Runs a for loop to determine the field with the most votes and returns it
    :return: returns the text value corresponding to the field with the most votes
    """
    # q = get_object_or_404(Question, pk=key)
    high_choice = 0
    high_text = None
    # i = 1
    for c in Choice.objects.filter(question=key):
        if c.votes > high_choice:
            high_choice = c.votes
            high_text = c.choice_text
			
			
    return high_text

