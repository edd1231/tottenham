from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static

from . import views
app_name="players"
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^add$', views.CreateView.as_view(success_url="/players/"), name='add'),
    url(r'players/(?P<pk>[0-9]+)/$', views.PlayerUpdate.as_view(success_url="/players/"), name='player_update'),
    url(r'^delete/(?P<pk>[0-9]+)/$', views.PlayerDelete.as_view(success_url="/players/"), name='player_delete'),
]