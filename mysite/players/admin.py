# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Player
import datetime
from django.contrib import admin

# Register your models here.
#class PlayerAdmin(admin.ModelAdmin):
    #list_display = 'forename'
    #fields = ['player_name','shirt_number','date_joined','player_age','player_position','goals_season']

admin.site.register(Player)