# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-09 16:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0005_auto_20170709_1501'),
    ]

    operations = [
        migrations.AlterField(
            model_name='player',
            name='player_name',
            field=models.CharField(max_length=200),
        ),
    ]
