# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-07 11:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('players', '0003_player_forename'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='player_age',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='player',
            name='shirt_number',
            field=models.IntegerField(default=0),
        ),
    ]
