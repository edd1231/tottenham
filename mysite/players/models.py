# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import datetime
from django.db import models

# Create your models here.

class Player(models.Model):
    player_name=models.CharField(max_length=200)
    forename = models.CharField(max_length=200)
    shirt_number=models.IntegerField(default=0)
    date_joined= models.DateField('When they joined the club')
    player_age=models.IntegerField(default=0)
    player_position = models.CharField(max_length=200)
    goals_this_season = models.IntegerField(default=0)
    def __str__(self):
        return str (self.player_name)
