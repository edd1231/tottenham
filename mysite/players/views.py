# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect
from .models import Player
from django.shortcuts import render, get_object_or_404, redirect
from django.views import generic
from django.urls import reverse_lazy

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'players/index.html'
    context_object_name = 'player_list'
    def get_queryset(self):
        """
        Return the player objects (not including those set to be
        published in the future).
        """
        return Player.objects.all()
class CreateView(generic.CreateView):
    template_name = "players/add_player.html"
    model = Player
    fields = "__all__"

class PlayerDelete(generic.DeleteView):
    model = Player
    template_name="players/confirm_delete.html"
# def player_delete(request, pk, template_name='players/confirm_delete.html'):
#     server = get_object_or_404(Player, pk=pk)
#     if request.method=='POST':
#         server.delete()
#         return redirect('IndexView', )
#     return render(request, template_name, {'object':server})

class PlayerUpdate (generic.UpdateView):
    model = Player
    fields="__all__"
    template_name = "players/update_form.html"
#writing a view as an edit
#class Editview(generic.)